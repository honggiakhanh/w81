import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyA5SfSoReaC9umy4qIFR98Z2UF99BjMxnI",
  authDomain: "w81final.firebaseapp.com",
  databaseURL:
    "https://w81final-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "w81final",
  storageBucket: "w81final.appspot.com",
  messagingSenderId: "624855048023",
  appId: "1:624855048023:web:ba0d5cda27916b0c342812",
};

const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);
