import { db } from "../firebase";
import { useState, useEffect } from "react";
import {
  collection,
  onSnapshot,
  doc,
  deleteDoc,
  updateDoc,
} from "firebase/firestore";

const TodoList = () => {
  const [todos, setTodos] = useState([]);

  const todoRef = collection(db, "todos");

  useEffect(() => {
    onSnapshot(todoRef, (snapshot) => {
      setTodos(
        snapshot.docs.map((todo) => {
          return {
            id: todo.id,
            ...todo.data(),
          };
        })
      );
    });
  }, []);
  
  return (
    <div>
      <div>To do list:</div>
      {todos.map((todo) => (
        <div key={todo.id}>
          <div>
            {" "}
            + {todo.title} ({todo.complete === true ? "Finished" : "Unfinished"}
            )
          </div>
          {!todo.complete && (
            <button
              onClick={() =>
                updateDoc(doc(db, "todos", todo.id), {
                  complete: !todo.complete,
                })
              }
            >
              To do finished!
            </button>
          )}
          <button onClick={() => deleteDoc(doc(db, "todos", todo.id))}>
            Delete todo
          </button>
        </div>
      ))}
    </div>
  );
};

export default TodoList;
