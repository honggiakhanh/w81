import { db } from "../firebase";
import { useState } from "react";
import { collection, addDoc } from "firebase/firestore";

const TodoForm = () => {
  const todoRef = collection(db, "todos");

  const [title, setTitle] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    const newTodo = {
      title,
      complete: false,
    };
    addDoc(todoRef, newTodo);

    setTitle("");
  };
  return (
    <form onSubmit={handleSubmit}>
      <div>Add new to do:</div>
      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      <button type="submit">Add to do</button>
    </form>
  );
};

export default TodoForm;
